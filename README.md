# vCard Network

This repository contains tools to allow the usage of publicly hosted vCards to
create a lightweight distributed alternative to a standard social network.

The main idea here is to combine the
[RSS](https://en.wikipedia.org/wiki/RSS)/[Atom](https://en.wikipedia.org/wiki/Atom_%28web_standard%29)
feed, which can provide updates on a person's activities, with a
[vCard](https://en.wikipedia.org/wiki/VCard), which provides a person's profile
in a way that can easily be distributed and locally stored (and synchronized
between devices through CardDAV).

The outcome is that you're able to have the list of people you're connected to
stored locally and follow updates from them in a completely decentralized way.

[Here](https://nfraprado.codeberg.page/vcf-test/main.html) you can see an
example page showing how one could show their profile and connections on their
website using a vCard and the scripts included in this repository.

## Usage

### Joining the network

In order to allow other people to connect to you and follow your updates:

1. Create your own vCard describing yourself. You can find an example vCard
   [here](https://codeberg.org/nfraprado/vcf-test/src/branch/pages/vcard.vcf).
   At the very least it should contain:
   * your name in `FN`
   * your website in `URL`
   * the URL where your vCard will be hosted in `SOURCE`
   * your website's RSS/Atom feed in the non-standard property [^1] `X-FEED`
2. Make your vCard publicly available on the URL you described in `SOURCE`.

### Connecting to someone

Before you can follow someone's updates you need to connect to them. To do so:

1. Download the person's vCard (ask them for the URL where it is stored) and
   store it in the folder where you keep all your connections.

### Following updates from your connections

1. Run the `vcard-to-opml` script contained in this repository and supply the
   vCards of all your connections to its input.
2. Import the OPML list generated from the script's output into your RSS/Atom
   feed reader of choice.

### Sharing your connections publicly

1. Run the `vcard-render` script with the `connections.html` template contained
   in this repository and supply the vCards of all your connections to its
   input.
2. Include the generated HTML output in your website.

### Displaying your vCard in your website

If you want you can use the data in your vCard to generate a profile page. You
can do it like this:

1. Run the `vcard-render` script using the `profile.html` (or tweak it to your
   needs) and supply your vCard to its input.
2. Include the generated output into your website.

[^1]: I really wanted the feed to be stored in a standard property of the vCard.
    I considered having it as a second `URL` property. Ultimately I realized
    that while a feed is an URL, the semantic, ie how it is handled by apps, is
    completely different, so different apps wouldn't be able to handle it even
    if they recognized the property. That's why I went for the non-standard
    `X-FEED` instead.

## License

BSD-3-Clause
