def in_preferred_order(property_list):
    """Return the list of entries for a property ordered based on the PREF
    attribute. Entries with no PREF are ordered last."""

    if len(property_list) < 2:
        return property_list

    def get_pref(prop):
        try:
            pref = prop.params['PREF'][0]
        except e:
            pref = 100 # Sort last if no PREF specified
        return pref

    return sorted(property_list, key=get_pref)

def in_language(property_list, language):
    """Return the list of entries for a property filtered so it only contains
    entries that have the LANGUAGE parameter set to a specific language.
    """
    def get_language(prop):
        try:
            return prop.params.get('LANGUAGE')[0]
        except TypeError:
            return ""

    return [prop for prop in property_list if get_language(prop) == language]

def in_lang_preferred(property_list, language):
    """Return the list of entries ordered by preference and, as long as
    non-empty, filtered by language.
    """
    prop_ordered = in_preferred_order(property_list)

    prop_filtered = in_language(prop_ordered, language)
    if len(prop_filtered) >= 1:
        return prop_filtered
    else:
        return prop_ordered
