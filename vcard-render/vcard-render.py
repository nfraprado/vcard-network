#!/bin/env python

import sys
import argparse

import vobject
import jinja2

from vcard_helpers import in_preferred_order, in_language, in_lang_preferred

argparser = argparse.ArgumentParser(
    prog="vcard-render",
    description="Render one or more vcards (.vcf files) using a template. vCards go into stdin. Rendered output comes out of stdout.",
)
argparser.add_argument("-t", "--template", required=True)

args = argparser.parse_args()

with open(args.template) as f:
    template = jinja2.Template(source=f.read())

# Expose helper functions to jinja template
template.globals.update(in_preferred_order=in_preferred_order,
                        in_language=in_language,
                        in_lang_preferred=in_lang_preferred)

vcards = [vcard.contents for vcard in vobject.readComponents(sys.stdin)]
print(template.render(vcards=vcards))
