#!/bin/env python

import sys
import argparse

import vobject
from opyml import OPML, Outline

argparser = argparse.ArgumentParser(
    prog="vcard-to-opml",
    description="Generate an OPML with the XML feeds from the X-FEED property of vCards (.vcf files). vCards go into stdin. OPML comes out of stdout.",
)
argparser.parse_args()

opml = OPML()

for vcard in vobject.readComponents(sys.stdin):
    opml.body.outlines.append(
        Outline(text=vcard.fn.value, title=vcard.fn.value, xml_url=vcard.x_feed.value)
    )

xml = opml.to_xml()
print(xml)
